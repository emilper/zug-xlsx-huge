#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Zug::XLSX::Huge' ) || print "Bail out!\n";
}

diag( "Testing Zug::XLSX::Huge $Zug::XLSX::Huge::VERSION, Perl $], $^X" );
