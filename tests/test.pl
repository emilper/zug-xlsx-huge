#!/usr/bin/env perl

use strict;
use warnings;

use Zug::XLSX::Huge;

my $columns = [
    { type => 'inlineStr', column_name => 'Some String' },
    { type => 'inlineStr', column_name => 'Some Other String' },
    { type => 'number',    column_name => 'A Number' },
    { type => 'number',    column_name => 'Another Number' },
];
my $filename = '/tmp/' . time() . '.xlsx';
print STDERR $filename, "\n";
my ( $worksheet_builder, $finish_workbook ) = Zug::XLSX::Huge::make_callbacks( {
    filename => $filename,
} );

my $worksheet_number = 1;    # first worksheet
my ( $add_row_to_first_worksheet, $finish_worksheet ) = $worksheet_builder->(
    $worksheet_number,
    {
        worksheet_name => 'first worksheet',
        columns        => $columns,
    } );

my $other_columns = [
    { type => 'inlineStr', column_name => 'Some String' },
    { type => 'inlineStr', column_name => 'Some Other String' },
    { type => 'number',    column_name => 'A Number' },
    { type => 'number',    column_name => 'Another Number' },
    { type => 'number',    column_name => 'Yet Another Number' },
    { type => 'number',    column_name => 'And Yet Another Number' },
];

#second worksheet
my ( $add_row_to_second_worksheet, $finish_second_worksheet ) = $worksheet_builder->(
    2,
    {
        worksheet_name => 'second worksheet',
        columns        => $other_columns,
    } );

my $max   = 100000;
my $count = 0;
while ( $count < $max ) {
    $add_row_to_first_worksheet->( 'a', 'b', $count, $count + 1 );
    $add_row_to_second_worksheet->( 'y', 'z', 30 % ( $count + 1 ), $count % 30, $count / 15, $count * 2 );
    $count++;
}

$finish_worksheet->();
$finish_second_worksheet->();

$finish_workbook->();
