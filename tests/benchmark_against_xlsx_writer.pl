#!/usr/bin/env perl

use strict;
use warnings;

use Zug::XLSX::Huge;
use Excel::Writer::XLSX;
use Benchmark qw(:all) ;
use Time::HiRes qw/time/;

my $max_rows = 10000;
cmpthese ( 100,
    { 
        'Zug::XLSX::Huge' => sub { build_with_me($max_rows); },
        'Excel::Writer::XLSX no optimization' => sub { build_with_xlsx_writer($max_rows); },
        'Excel::Writer::XLSX with optimization' => sub { build_with_xlsx_writer($max_rows, 1); },
    }
);

sub build_with_me {
    my $max_rows = shift;
    my ( $worksheet_builder, $finish_workbook ) = Zug::XLSX::Huge::make_callbacks( {
        filename => '/tmp/mine_' . time() . '.xlsx',
    } );

    my $worksheet_number = 1;    # first worksheet
    my $columns = [
        { type => 'inlineStr', column_name => 'Some String' },
        { type => 'inlineStr', column_name => 'Some Other String' },
        { type => 'number',    column_name => 'A Number' },
        { type => 'number',    column_name => 'Another Number' },
        { type => 'number',    column_name => 'Yet Another Number' },
        { type => 'number',    column_name => 'And Yet Another Number' },
    ];

    #second worksheet
    my ( $add_row_to_worksheet, $finish_worksheet ) = $worksheet_builder->(
        $worksheet_number,
        {
            worksheet_name => 'my worksheet',
            columns        => $columns,
        } );

    my $count = 0;
    for my $count ( 0 .. $max_rows ) {
        $add_row_to_worksheet->( 'y', 'z', 30 % ( $count + 1 ), $count % 30, $count / 15, $count * 2 );
        $count++;
    }

    $finish_worksheet->();
    $finish_workbook->();
}

sub build_with_xlsx_writer {
    my $max_rows = shift;
    my $optimize = shift;
    my $prefix = $optimize ? 'optimized_' : 'not_optimized_';
    # Create a new Excel workbook
    my $workbook = Excel::Writer::XLSX->new( '/tmp/' . $prefix . time() . '.xlsx' );
    $workbook->set_optimization() if $optimize;
    # Add a worksheet
    my $worksheet = $workbook->add_worksheet();

    for my $i ( 0 .. $max_rows ) {
        my $row = [ 'y', 'zy', 30 % ( $i + 1 ), $i % 30, $i / 15, $i + 2 ];
        $worksheet->write_row( $i, 0, $row );
    }

    $workbook->close();
}
