#!/usr/bin/env bash

export ZUG_DEBUG=1
export PERL5LIB=./lib/:$PERL5LIB
for i in `ls ./tests/*.pl`
do
    perl -wc $i
    perl -w  $i
done
