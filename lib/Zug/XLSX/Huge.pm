package Zug::XLSX::Huge;

use strict;
use warnings;

use File::Temp;
use File::Path qw/make_path/;
use File::Spec;
use Archive::Zip;
use String::Util;
use DateTime;
use DateTime::Format::W3CDTF;

my $build_cell = {
    'inlineStr' => \&build_inline_string_cell,
    'number'    => \&build_number_cell,
};

sub check_temp_dir {
    my ( $args ) = @_;

    if ( exists $args->{temp_dir} && defined $args->{temp_dir} ) {
        my $temp_dir = $args->{temp_dir};
        Zug::Exceptions::temporary_folder_not_found( $temp_dir ) unless -d $temp_dir;
        die "cannot write to the temporary folder: " . $temp_dir unless -w $temp_dir;
        return File::Temp::tempdir( DIR => $temp_dir, CLEANUP => 1 );
    }

    return File::Temp::tempdir( DIR => File::Spec->tmpdir, CLEANUP => 1 );
}

sub make_callbacks {
    my ( $args ) = @_;

    my $valid_args = {
        filename => 1,
        temp_dir => 1,

        # TODO automatic split in worksheets not done yet
        # auto_split => 1,
    };

    foreach my $key ( keys %$args ) {

        # TODO this is not yet an exception, just dies with a message
        Zug::Exceptions::invalid_attribute( $key ) unless exists $valid_args->{$key};
    }

    my $tempdir                = check_temp_dir( $args );
    my $global_rels_path       = $tempdir . '/_rels';
    my $global_rels_xml_path   = $global_rels_path . '/.rels';
    my $worksheets_path        = $tempdir . '/xl/worksheets';
    my $workbook_path          = $tempdir . '/xl/workbook.xml';
    my $content_type_path      = $tempdir . '/[Content_Types].xml';
    my $app_xml_path           = $tempdir . '/docProps/app.xml';
    my $core_xml_path          = $tempdir . '/docProps/core.xml';
    my $workbook_rels_path     = $tempdir . '/xl/_rels';
    my $workbook_xml_rels_path = $workbook_rels_path . '/workbook.xml.rels';

    make_path( $tempdir . '/xl' );
    make_path( $tempdir . '/docProps' );
    make_path( $global_rels_path );
    make_path( $workbook_rels_path );
    make_path( $worksheets_path );

    write_content_type_file( $content_type_path );
    write_doc_props_app_xml( $app_xml_path );
    write_doc_props_core_xml( $core_xml_path );
    write_global_rels( $global_rels_xml_path );
    write_rels_doc_start( $workbook_xml_rels_path );
    write_workbook_start( $workbook_path );

    my $worksheet_builder = sub {
        my $worksheet_count = shift;
        my $args            = shift;

        my $valid_args = {
            columns             => 1,
            row_cache_max_count => 1,
            worksheet_name      => 1,
        };

        foreach my $key ( keys %$args ) {

            # TODO this is not yet an exception, just dies with a message
            Zug::Exceptions::invalid_attribute( $key ) unless exists $valid_args->{$key};
        }

        my $worksheet_name     = worksheet_name( $args->{worksheet_name} );
        my $worksheet_xml_path = $worksheets_path . '/sheet' . $worksheet_count . '.xml';
        add_worksheet_to_workbook_xml( $workbook_path, $worksheet_name, $worksheet_count );
        add_worksheet_to_doc_rels( $workbook_xml_rels_path, $worksheet_count );
        write_worksheet_start( $worksheet_xml_path );

        my $row_count = 1;    # excel rows are counted from 1 not from 0

        my ( $header_columns, $header_values ) = extract_headers( $args->{columns} );

        my $headers_row = build_row( $row_count, $header_columns, @$header_values );
        Zug::Utils::append_to_file( $worksheet_xml_path, $headers_row );
        $row_count++;

        my @rows_buffer;
        my $max_rows_in_buffer = 5000;
        my $add_row            = sub {
            my ( @row_data ) = @_;
            push @rows_buffer, build_row( $row_count, $args->{columns}, @row_data );
            if ( $#rows_buffer > $max_rows_in_buffer ) {
                Zug::Utils::append_to_file( $worksheet_xml_path, @rows_buffer );
                undef @rows_buffer;    # clear the buffer; TODO: check this frees the memory
            }

            $row_count++;
        };

        my $finish_worksheet = sub {

            # clear the cache if anything left inside
            if ( scalar( @rows_buffer ) > 0 ) {
                Zug::Utils::append_to_file( $worksheet_xml_path, @rows_buffer );
                undef @rows_buffer;    # clear the buffer; TODO: check this frees the memory
            }

            write_worksheet_end( $worksheet_xml_path );
        };

        return ( $add_row, $finish_worksheet );
    };

    my $finish_workbook = sub {

        write_rels_doc_end( $workbook_xml_rels_path );
        write_workbook_end( $workbook_path );

        make_xlsx_file( $args->{filename}, $tempdir );
    };

    return ( $worksheet_builder, $finish_workbook );
}

sub worksheet_name {
    my ( $string, $sheet_count ) = @_;
    $string      = String::Util::trim( $string );
    $sheet_count = String::Util::trim( $sheet_count );
    $sheet_count = $sheet_count ? $sheet_count : 1;
    my $name = $string ? $string : 'Sheet' . $sheet_count;

    # http://stackoverflow.com/questions/3681868/is-there-a-limit-on-an-excel-worksheets-name-length
    # apparenty maximum number of characters is 31
    Zug::Exceptions::worksheet_name_too_long( $string ) if length( $name ) > 31;

    return $name;
}

sub make_xlsx_file {
    my ( $filename, $tmpdir ) = @_;
    my $zip = Archive::Zip->new();
    $zip->addTree( $tmpdir );
    $zip->writeToFileNamed( $filename );
}

sub extract_headers {
    my $columns = shift;
    my @types;
    my @values;
    foreach my $column_data ( @$columns ) {
        push @types, { type => 'inlineStr' };

        # SEEME: do I need this ? maybe I don't want a headers row
        Zug::Exceptions::invalid_attribute( $column_data ) unless exists $column_data->{column_name};
        push @values, $column_data->{column_name};
    }
    return ( \@types, \@values );
}

# TODO test this
sub should_add_headers_row {
    my @not_empty_headers = grep { defined $_ && $_ ne '' } @_;    # find values that are not empty, that is not undef and not ''
    return scalar( @not_empty_headers );
}

sub build_row {
    my ( $row_count, $columns, @data ) = @_;

    my $cell_count = 1;                                            # excel indices start at 1
    my $row        = '<row r="' . $row_count . '">';
    my $column_id  = 'A';
    foreach my $cell_data ( @data ) {
        my $column_definition   = @$columns[ $cell_count - 1 ];
        my $cell_type           = $column_definition->{type};
        my $build_cell_function = ( exists( $build_cell->{$cell_type} ) ) ? $build_cell->{$cell_type} : $build_cell->{number};
        $row .= $build_cell_function->( $column_id . $row_count, $cell_data );
        $cell_count++;
        $column_id++;    # perl -e 'my $test = "A"; my $count = 0; while ($count < 10) { print $test, "\n"; $test++; $count++}'
    }

    $row .= "</row>\n";

    return $row;
}

sub build_cell {
    my ( $cell_id, $column_definition, $cell_data ) = @_;

    if ( $column_definition->{type} eq 'inlineStr' ) {
        return '<c r="' . $cell_id . '" t="inlineStr">
	    <is><t>' . $cell_data . '</t></is>
	</c>
';
    } elsif ( !exists( $column_definition->{type} )
        || !defined( $column_definition->{type} )
        || $column_definition->{type} eq 'number' ) {
        return '<c r="' . $cell_id . '"><v>' . $cell_data . '</v></c>' . "\n";
    } elsif ( $column_definition->{type} eq 's' ) {
        die "died horribly, finish me TODO\n";
    }
}

sub build_number_cell {
    return '<c r="' . $_[0] . '"><v>' . $_[1] . '</v></c>
';
}

sub build_inline_string_cell {
    return '<c r="' . $_[0] . '" t="inlineStr">
	<is><t>' . $_[1] . '</t></is>
    </c>
';
}

sub build_shared_string_cell {
    die "finish me TODO\n";
}

sub write_content_type_file {
    my $file_name = shift;
    my $string    = '<?xml version="1.0" encoding="UTF-8"?>
<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">
<Override PartName="/_rels/.rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>
<Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml"/>
<Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml"/>
<Override PartName="/xl/_rels/workbook.xml.rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>
<Override PartName="/xl/sharedStrings.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml"/>
<Override PartName="/xl/worksheets/sheet.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml"/>
<Override PartName="/xl/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"/>
<Override PartName="/xl/workbook.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"/>
</Types>';
    Zug::Utils::append_to_file( $file_name, $string );
}

sub write_doc_props_app_xml {
    my $file_name = shift;
    my $string    = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Properties
    xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
    xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
    <TotalTime>0</TotalTime>
    <Application>Zug::XLSX::Huge</Application>
</Properties>';

    Zug::Utils::append_to_file( $file_name, $string );
}

sub write_doc_props_core_xml {
    my $file_name     = shift;
    my $w3cdtf_parser = DateTime::Format::W3CDTF->new();
    my $created       = $w3cdtf_parser->format_datetime( DateTime->now() );

    my $string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<cp:coreProperties
    xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:dcmitype="http://purl.org/dc/dcmitype/"
    xmlns:dcterms="http://purl.org/dc/terms/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <dcterms:created xsi:type="dcterms:W3CDTF">' . $created . '</dcterms:created>
    <dcterms:modified xsi:type="dcterms:W3CDTF">' . $created . '</dcterms:modified>
    <cp:revision>1</cp:revision>
</cp:coreProperties>';

    Zug::Utils::append_to_file( $file_name, $string );
}

sub write_workbook_start {
    my $file_name = shift;
    my $string    = '<?xml version="1.0" encoding="UTF-8"?>
<workbook
    xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
    xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">
  <fileVersion lastEdited="4" lowestEdited="4" rupBuild="3820"/>
  <workbookPr date1904="0"/>
  <bookViews>
    <workbookView activeTab="1"/>
  </bookViews>
  <sheets>
';

    Zug::Utils::append_to_file( $file_name, $string );
}

sub add_worksheet_to_workbook_xml {
    my ( $file_name, $sheet_name, $sheet_id ) = @_;
    my $string = '
    <sheet name="' . $sheet_name . '" sheetId="' . $sheet_id . '" r:id="rId' . ( $sheet_id + 2 ) . '"/>
';

    # <sheet name="'. $sheet_name . '" sheetId="' . $sheet_id . '" state="visible" r:id="rId1"/>
    Zug::Utils::append_to_file( $file_name, $string );
}

sub add_worksheet_to_doc_rels {
    my $file_name    = shift;
    my $worksheet_id = shift;

    my $string =
          '    <Relationship Id="rId'
        . ( $worksheet_id + 2 )
        . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" Target="worksheets/sheet'
        . $worksheet_id
        . '.xml"/>
';
    Zug::Utils::append_to_file( $file_name, $string );
}

sub write_workbook_end {
    my $file_name = shift;

    # definedNames might not be needed
    #  </sheets>
    #<definedNames>
    #    <definedName name="_xlnm.Print_Area" localSheetId="0">#REF!</definedName>
    #    <definedName name="_xlnm.Sheet_Title" localSheetId="0">&quot;test sheet&quot;</definedName>
    #</definedNames>
    # <calcPr calcMode="auto" iterate="1" iterateCount="100" iterateDelta="0.001"/>

    # calcPr details here https://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.calculationproperties(v=office.15).aspx
    my $string = '  </sheets>
  <calcPr calcMode="auto" iterate="1" iterateCount="100" iterateDelta="0.001"/>
  <webPublishing allowPng="1" css="0" characterSet="UTF-8"/>
</workbook>';

    Zug::Utils::append_to_file( $file_name, $string );
}

sub write_global_rels {
    my $file_name = shift;
    my $string    = q{<?xml version="1.0" encoding="UTF-8"?>
<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
  <Relationship Id="rId3" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>
  <Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>
  <Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="xl/workbook.xml"/>
</Relationships>
};
    Zug::Utils::write_file( $file_name, $string );
}

sub write_rels_doc_start {
    my $file_name = shift;
    my $string    = '<?xml version="1.0" encoding="UTF-8"?>
<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>
<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings" Target="sharedStrings.xml"/>
';

    Zug::Utils::write_file( $file_name, $string );
}

sub write_rels_doc_end {
    my $file_name = shift;
    my $string    = '</Relationships>';

    Zug::Utils::append_to_file( $file_name, $string );
}

sub write_worksheet_start {
    my $file_name = shift;

    my $string = q{<?xml version="1.0" encoding="UTF-8"?>
<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">
  <dimension ref="A1:A4"/>
  <sheetFormatPr defaultRowHeight="12.75"/>
  <cols>
    <col min="1" max="1" width="9.142308"/>
  </cols>
  <sheetData>
};

    Zug::Utils::append_to_file( $file_name, $string );
}

sub write_worksheet_end {
    my $file_name = shift;
    my $string    = q{  </sheetData>
  <sheetProtection formatCells="0" formatColumns="0" formatRows="0" insertColumns="0" insertRows="0" insertHyperlinks="0" deleteColumns="0" deleteRows="0" selectLockedCells="1" sort="0" autoFilter="0" pivotTables="0" selectUnlockedCells="1"/>
  <printOptions/>
  <pageMargins left="1" right="1" top="1.667" bottom="1.667" header="1" footer="1"/>
  <pageSetup/>
</worksheet>    
};
    Zug::Utils::append_to_file( $file_name, $string );
}

package Zug::Exceptions;

use strict;
use warnings;

#TODO: not real exceptions

sub invalid_attribute {
    my ( $attr ) = @_;

    die "invalid attribute " . $attr;
}

sub worksheet_name_too_long {
    my ( $worksheet_name ) = @_;
    die "Worksheet name " . $worksheet_name . " is too long: maximum length accepted by Excel is 31 characters.";
}

sub temporary_folder_not_found {
    my ( $temp_dir ) = @_;
    die "temporary folder not found: " . $temp_dir;
}

package Zug::Log;

use strict;
use warnings;
use feature "say";

use Exporter;
our @EXPORT_OK = qw(shout);

#TODO: lots to do, including moving to another module distribution
sub shout {
    my $message = shift;

    foreach my $d ( @_ ) {
        if ( ref( $d ) ) {
            $message .= Dumper( $d ) . "\n";
        } else {
            $message .= $d . "\n";
        }
    }
    print STDERR $message;    # TODO add some date, proc ID etc.
}

package Zug::Utils;

#TODO: lots to do, including moving to another module distribution

use strict;
use warnings;

use Exporter;
our @EXPORT_OK = qw/append_to_file write_file/;

sub append_to_file {
    my ( $file_name, @strings ) = @_;

    open( my $fh, '>>:encoding(UTF-8)', $file_name ) or die $!;
    print $fh @strings;
    close( $fh );
}

sub write_file {
    my ( $file_name, $string ) = @_;

    open( my $fh, '>:encoding(UTF-8)', $file_name ) or die $!;
    print $fh $string;
    close( $fh );
}

1;

=head1 NAME

Zug::XLSX::Huge - build large xlsx files without running out of RAM

=head1 VERSION

Version 0.02

=cut

our $VERSION = '0.02';

=head1 SYNOPSIS

    #!/usr/bin/env perl

    use strict;
    use warnings;

    use Zug::XLSX::Huge;

    my $columns = [
        { type => 'inlineStr', column_name => 'Some String' },
        { type => 'inlineStr', column_name => 'Some Other String' },
        { type => 'number',    column_name => 'A Number' },
        { type => 'number',    column_name => 'Another Number' },
    ];

    my ( $worksheet_builder, $finish_workbook ) = Zug::XLSX::Huge::make_callbacks( {
        filename       => '/tmp/' . time() . '.xlsx',
    } );

    my $worksheet_number = 1; # first worksheet
    my ( $add_row_to_first_worksheet, $finish_worksheet ) = $worksheet_builder->($worksheet_number, {
        worksheet_name => 'first worksheet',
        columns        => $columns,
    });


    my $other_columns = [
        { type => 'inlineStr', column_name => 'Some String' },
        { type => 'inlineStr', column_name => 'Some Other String' },
        { type => 'number',    column_name => 'A Number' },
        { type => 'number',    column_name => 'Another Number' },
        { type => 'number',    column_name => 'Yet Another Number' },
        { type => 'number',    column_name => 'And Yet Another Number' },
    ];

    #second worksheet
    my ( $add_row_to_second_worksheet, $finish_second_worksheet ) = $worksheet_builder->(2, {
        worksheet_name => 'second worksheet',
        columns        => $other_columns,
    });

    my $max   = 100;
    my $count = 0;
    while ( $count < $max ) {
        $add_row_to_first_worksheet->( 'a', 'b', $count, $count + 1 );
        $add_row_to_second_worksheet->('y', 'z', 30 % ($count + 1) , $count % 30, $count/15, $count*2);
        $count++;
    }

    $finish_worksheet->();
    $finish_second_worksheet->();

    $finish_workbook->();


=head1 STATUS

Zug::XLSX::Huge is in early alpha stage, there are no unit tests or automatic tests, just a test script which must be run manually; no optimization
attempted so far. Some xml elements in the files generated might not be really needed and will be removed.

=head1 DESCRIPTION

I don't intend to extend it to be a competitor for Excel::Writer::XLSX and Zug::XLSX::Huge will always build very basic XLSX files.
If you need to build XLSX files please use Excel::Writer::XLSX. Excel::Writer::XLSX also has the C<set_optimization()> method to turn 
on RAM usage optimization with a few drawbacks which also apply to this module.

I wrote Zug::XLSX::Huge to answer to a very special need: building huge excel files while using a predictable amount of RAM. To achieve
this the files will not contain shared strings (unless you already know which strings should be shared -- feature still pending) and
the files generated will most likely be larger than they could be when built with Excel::Writer::XLSX.


=head1 LIMITATIONS

As of now Zug::XLSX::Huge supports only numeric values and inline strings. No formatting is possible.


=head2 TODO

=over

=item split data automatically over multiple worksheets when a row count is reached

=item shared strings

=item row bufferring

=item controlled RAM usage

=item basic cell formatting (very low priority feature)

=item formulas (very low priority feature)

=back

=head1 AUTHOR

Emil Nicolaie Perhinschi, C<< <emilper at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests at https://github.com/emil-perhinschi/zug-xlsx-huge.


=head1 LICENSE AND COPYRIGHT

Copyright 2015 Emil Perhinschi.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut
